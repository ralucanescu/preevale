// Set height for main & section of each page on window load + resize
$(window, document).on('load resize', function() {
	
  // Declare variables and functions
	var trigger = $('.nav-item a');
	var location = window.location.hash;
	var sectionHeight = $(location + '.panel .container').outerHeight(true);
	var homeHeight = $('#home.container').outerHeight(true);
	var fixHeight = $('footer').outerHeight(true) + $('header').outerHeight(true);
	//Functions
	function setHeight() {
	setTimeout(function(){
			$('main').css('height', sectionHeight);
			$('body').css('height', sectionHeight + fixHeight);
		  },800);
	}
	function setHomeHeight() {
		$('main').css('height', homeHeight);
		$('body').css('height', homeHeight).css('height', '+=' + fixHeight);
	}
	
	// Set main + body height on page load
	if (location === '#home' || location === '') {
		setHomeHeight();
	} else {
		setHeight();
	}
	// Set main + body height on click
	trigger.on('click', function(){
	  $('html, body').animate({scrollTop: '0px'}, 300);
		var target = $(this).data('target');
		sectionHeight = $('#' + target + '.panel .container').outerHeight(true);
		
		if (target === 'home') {
			setHomeHeight();
		} else {
			setHeight();
		}
	});
});

$(document).ready(function(){
	$('html, body').animate({scrollTop: '0px'}, 300);

	//Trigger datetimepicker
	$( "#datepicker" ).datetimepicker({
		controlType: 'select',
		oneLine: true
	});
	
	//Change color of default selected option
	$('#prefix').css('color', '#b6b6b6');
	$('#prefix').change(function() {
		if ($('option:not(disabled)')) {
			$("#prefix").css('color', '#000')
		} else {
			$('#prefix').css('color', '#b6b6b6');
		}
	});
	
	//Mobile menu
	$('.mobile-menu-trigger').on('click', function(e) {
		$(this).toggleClass('active');
		$('.mobile-menu-wrapper').toggleClass('active');
	});
	$('.nav-item').on('click', function() {
		$('.mobile-menu-trigger').removeClass('active');
		$('.mobile-menu-wrapper').removeClass('active');
	});
	
	//Login button
	$('.trigger-login').on('click', function() {
		$('body').addClass('login-popup');
		$('#login_name').focus();
	});
	$('#popup-close').on('click', function() {
		$('body').removeClass('login-popup');
	});
	//Scroll to top button
	$(window).scroll(function() {
        if ($(this).scrollTop() > 500) {
            $('.go-top').fadeIn(200);
        } else {
            $('.go-top').fadeOut(200);
        }
    });
	$('.go-top').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
		var $target = $('#top');
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing');
    });
});