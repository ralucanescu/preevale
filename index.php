	<?php include 'components/header.html';?>
	<main>
		<!-- Home -->
		<section id="home" class="container">
			<?php include 'components/home.html';?>
		</section>
		<!-- /Home -->
		<!-- Team -->
		<section id="team" class="panel">
			<div class="container">
				<?php include 'components/team.html';?>
			</div>
		</section>
		<!-- /Team -->
		<!-- Services -->
		<section id="services" class="panel">
			<div class="container">
				<?php include 'components/services.html';?>
			</div>
		</section>
		<!-- /Services -->
		<!-- Contact -->
		<section id="contact" class="panel">
			<div class="container">
				<?php include 'components/contact.html';?>
			</div>
		</section>
		<!-- /Contact -->
	</main>
	<?php include 'components/footer.html';?>
</body>
</html>